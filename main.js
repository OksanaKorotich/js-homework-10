'use strict';

let tabsTitle;
let tabContent;


window.onload = function() {
    tabsTitle = document.querySelectorAll('.tabs-title');
    tabContent = document.querySelectorAll('.tab-content');

    hideContent(1);
}


function hideContent(a) {
    for (let i = a; i < tabContent.length; i++) {
        tabContent[i].classList.remove('show');
        tabContent[i].classList.add('hide');
        tabsTitle[i].classList.remove('active');
    };
};


document.getElementById('tabs').onclick = function(event){
    let target = event.target;
    if (target.className =='tabs-title') {
       for (let i = 0; i < tabsTitle.length; i++) {
           if (target == tabsTitle[i]) {
               showContent(i);
               break;
            };
        };
    };
};


function showContent(item){
    if (tabContent[item].classList.contains('hide')) {
        hideContent(0);
        tabsTitle[item].classList.add('active');
        tabContent[item].classList.remove('hide');
        tabContent[item].classList.add('show');
    }
}

